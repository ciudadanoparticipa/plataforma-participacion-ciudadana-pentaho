#!/bin/bash

if [ -n "$1" ]; then
  PENTAHO_DIR=$1
else
  PENTAHO_DIR=$PWD/jobs
fi

chmod 555 $PENTAHO_DIR

docker run --rm -ti -v $PENTAHO_DIR:/home/pentaho/repo --network host mvnfenzan/pentaho-di -file /home/pentaho/repo/etl_censo.kjb
